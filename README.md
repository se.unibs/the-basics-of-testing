# The basics of testing

In order to work with the code contained in this repository, you should

1. obtain the source code:
    * clone the repository using git command `git clone git@gitlab.com:se.unibs/the-basics-of-testing.git` or `git clone https://gitlab.com/se.unibs/the-basics-of-testing.git` or
    * download the repo's code as zip file clicking [here](https://gitlab.com/se.unibs/the-basics-of-testing/-/archive/master/the-basics-of-testing-master.zip)
2. open you preferred IDE (maybe Eclipse or IntelliJIDEA)
3. import the project as _Gradle project_, following the wizard of the IDE of your choice
4. run tests (bar should be green)
5. enjoy changing values around (see comments) and re-running tests