package it.unibs.ing.se.testing.basics;

public class SystemUnderTest {
    public static String hello() {
        return "Hello, World!";
    }

    public static int sum(int x, int y) {
        return x + y;
    }

    public static String unsafeSubstring(String s, int begin, int end) {
        return s.substring(begin, end);
    }
}
