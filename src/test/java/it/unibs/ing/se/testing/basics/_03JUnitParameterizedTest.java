package it.unibs.ing.se.testing.basics;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class _03JUnitParameterizedTest {
    private final int x;
    private final int y;
    private final int sum;

    // Receive constructors parameters containing values from Collection returned by @Parameterized.Parameters annotated method
    // One test case for each element in the returned Collection<Object[]>
    // For each test case, one constructor parameter (positional) for each element in the Object<[]>
    public _03JUnitParameterizedTest(int x, int y, int sum) {
        this.x = x;
        this.y = y;
        this.sum = sum;
    }

    @Test
    public void shouldSumIntegersCorrectly() {
        // Express assertions as usual...
        assertThat(SystemUnderTest.sum(x, y), is(equalTo(sum)));
    }

    // Try to change test case output format, e.g. using something like "{index}: {0} + {1} should be {2}"
    @Parameterized.Parameters(name = "{index}: Test with x={0}, y ={1}, result (x + y) is:{2} ")
    public static Collection<Object[]> data() {
        // Try to add more test cases, e.g. 11 + 19 = 30, 100 + 100 = 200, 19 + 0 = 19, -19 + 11 = 8, ...
        // Try to add a wrong test case, e.g. 11 + 19 = 40
        Object[][] data = new Object[][]{{1, 2, 3}, {5, 3, 8}, {121, 40, 161}};
        return Arrays.asList(data);
    }
}
