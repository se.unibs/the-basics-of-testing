package it.unibs.ing.se.testing.basics;

import org.junit.Test;

import static org.junit.Assert.*;

public class _01JUnitBasicsTest {
    @Test
    public void canCompareStrings() {
        // Try to change asserted value, e.g. to "Hello, Software Engineering!"
        assertEquals("Hello, World!", SystemUnderTest.hello());
    }

    @Test
    public void canCompareIntegers() {
        // Try to change asserted value, e.g. to 19
        assertEquals(13, SystemUnderTest.hello().length());
    }

    @Test
    public void canCompareObjects_SameObject() {
        Object o = new Object();
        Object o1 = o;
        assertEquals(o, o1);
        // Try to compare two different Objects
        //Object o2 = new Object();
        //assertEquals(o, o2);
    }

    @Test
    public void canCompareObjects_SameValue() {
        Text txt0 = new Text("aaa");
        Text txt1 = new Text("aaa");
        // Compared Objects are different but the Text class implement equals/hashCode: assertion passes
        assertTrue(txt0.equals(txt1));
    }

    @Test
    public void canCompareArrays() {
        int[] values0 = new int[] {1, 2, 3};
        int[] values1 = new int[] {1, 2, 3};
        // Try to change expected value 'values0' to {1, 2, 3, 4} or {1, 2, 4}
        assertArrayEquals(values0, values1);
    }

    @Test(expected = NullPointerException.class)
    public void canMakeAssertionOnExceptionsThrown() {
        // Try to change expected exception to IllegalArgumentException (red test) or Exception (green test: why?)
        SystemUnderTest.unsafeSubstring(null, 0, 2);
    }

    @Test(timeout = 500L)
    public void canSpecifyTimeout() throws InterruptedException {
        // Try to change specified timeout to 200 milliseconds
        Thread.sleep(400L);
    }
}
