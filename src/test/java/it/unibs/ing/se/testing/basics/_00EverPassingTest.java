package it.unibs.ing.se.testing.basics;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class _00EverPassingTest {
    @Test
    public void shouldEverPass() {
        Assert.assertTrue(true);
        // Using static import...
        assertTrue(true);
    }
//  Try changing asserted value from true to false
//    @Test
//    public void shouldNeverPass() {
//        Assert.assertTrue(false);
//    }

//    @Test
//    public void shouldFailEver() {
//        Assert.fail();
//    }
}
