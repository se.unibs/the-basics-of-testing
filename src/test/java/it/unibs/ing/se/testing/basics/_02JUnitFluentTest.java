package it.unibs.ing.se.testing.basics;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class _02JUnitFluentTest {
    @Test
    public void canCompareStrings() {
        // Same as _01JUnitBasicsTest.canCompareStrings, but the assertion is expressed in a more readable fashion
        //  (something similar to an english sentence)
        assertThat(SystemUnderTest.hello(), is(equalTo("Hello, World!")));
    }

    @Test
    public void canCompareIntegers() {
        // Try to use other Matcher builder static methods, like greaterThan and similar...
        //  http://hamcrest.org/JavaHamcrest/javadoc/1.3/org/hamcrest/Matchers.html
        assertThat(SystemUnderTest.hello().length(), is(equalTo(13)));
        assertThat(SystemUnderTest.hello().length(), is(greaterThan(11)));
        assertThat(SystemUnderTest.hello().length(), is(lessThanOrEqualTo(19)));
    }

    @Test
    public void canCompareObjects_SameObject() {
        String s = "A text";
        Object x = s;
        assertThat(x, is(equalTo(s)));
    }

    @Test
    public void canCompareObjects_SameValue() {
        Text txt0 = new Text("aaa");
        Text txt1 = new Text("aaa");
        assertThat(txt1, is(equalTo(txt0)));
    }

    @Test
    public void canCompareArrays() {
        Integer[] values0 = new Integer[] {1, 2, 3};
        Integer[] values1 = new Integer[] {1, 2, 3};
        assertThat(values1, is(equalTo(values0)));
        // Try to use Hamcrest's Matchers like contains, containsInAnyOrder, isIn, ...
        List<Integer> list = Arrays.asList(values0);
        assertThat(list, contains(1, 2, 3));
        assertThat(list, containsInAnyOrder(2, 3, 1));
        assertThat(list, containsInAnyOrder(2, 1, 3));
        assertThat(list, hasItem(3));
        assertThat(3, isIn(values0));
    }
}
